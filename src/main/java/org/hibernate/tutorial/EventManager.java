package org.hibernate.tutorial;

import org.hibernate.*;
import org.hibernate.tutorial.domain.*;
import org.hibernate.tutorial.util.*;

import java.util.*;

/**
 * Created by florian on 18/04/14.
 */
public class EventManager {

    private final static String LIST = "list",
            STORE = "store",
            ADD_PERSON_TO_EVENT = "addpersontoevent",
            ADD_EMAIL_ADDRESS_TO_PERSON = "addemailaddresstoperson";

    public static void main(String[] args) {

        if (args.length > 0) {

            final EventManager eventManager = new EventManager();
            final String cmd = args[0];

            if (ADD_EMAIL_ADDRESS_TO_PERSON.equals(cmd)) {
                Long personId = eventManager.createAndStorePerson(34, "Foo", "Bar");
                eventManager.addEmailToPerson(personId, "test@gmx.de");
            } else if (ADD_PERSON_TO_EVENT.equals(cmd)) {
                Long eventId = eventManager.createAndStoreEvent("My Event", new Date());
                Long personId = eventManager.createAndStorePerson(34, "Foo", "Bar");
                eventManager.addPersonToEvent(personId, eventId);
                System.out.println("Added person " + personId + " to event " + eventId);
            } else if (STORE.equals(cmd)) {
                eventManager.createAndStoreEvent("My Event", new Date());
            } else if (LIST.equals(cmd)) {
                List events = eventManager.listEvents();
                for (Object event : events) {
                    Event theEvent = (Event) event;
                    System.out.println("Event: " + theEvent.getTitle() + " Time: " + theEvent.getDate());
                }
            }
        }

        HibernateUtil.getSessionFactory().close();
    }

    private void addPersonToEvent(final Long personId,
                                  final Long eventId) {
        final Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();

        final Person person = (Person) session.load(Person.class, personId);
        final Event event = (Event) session.load(Event.class, eventId);
        person.getEvents().add(event);
        session.getTransaction().commit();
    }

    private List listEvents() {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        List result = session.createQuery("from Event").list();
        session.getTransaction().commit();
        return result;
    }

    private Long createAndStoreEvent(String title,
                                     Date date) {

        final Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();

        Event event = new Event();
        event.setDate(date);
        event.setTitle(title);
        session.save(event);

        session.getTransaction().commit();

        return event.getId();
    }

    private Long createAndStorePerson(final int age,
                                      final String firstName,
                                      final String lastName) {

        final Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();

        Person person = new Person();
        person.setAge(age);
        person.setFirstName(firstName);
        person.setLastName(lastName);
        person.setFirstName(firstName);
        session.save(person);

        session.getTransaction().commit();

        return person.getId();
    }

    private void addEmailToPerson(Long personId,
                                  String emailAddress) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();

        Person aPerson = (Person) session.load(Person.class, personId);
        // adding to the emailAddress collection might trigger a lazy load of the collection
        aPerson.getEmailAddresses().add(emailAddress);

        session.getTransaction().commit();
    }
}
