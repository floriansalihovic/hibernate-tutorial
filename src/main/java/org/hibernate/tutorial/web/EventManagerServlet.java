package org.hibernate.tutorial.web;

import org.hibernate.*;
import org.hibernate.tutorial.domain.*;
import org.hibernate.tutorial.util.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.text.*;
import java.util.*;

/**
 * Created by florian on 19/04/14.
 */
public class EventManagerServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {

        final DateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
        final Transaction transaction = HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();
        try {
            // Begin unit of work

            // Write HTML header
            PrintWriter out = response.getWriter();
            out.println("<html><head><title>Event Manager</title></head><body>");

            // Handle actions
            if ("store".equals(request.getParameter("action"))) {

                String eventTitle = request.getParameter("eventTitle");
                String eventDate = request.getParameter("eventDate");

                if ("".equals(eventTitle) || "".equals(eventDate)) {
                    out.println("<b><i>Please enter event title and date.</i></b>");
                } else {
                    createAndStoreEvent(eventTitle, dateFormatter.parse(eventDate));
                    out.println("<b><i>Added event.</i></b>");
                }
            }

            // Print page
            printEventForm(out);
            listEvents(out, dateFormatter);

            // Write HTML footer
            out.println("</body></html>");
            out.flush();
            out.close();

            // End unit of work
            transaction.commit();
        } catch (Exception ex) {
            transaction.rollback();
            if (ServletException.class.isInstance(ex)) {
                throw (ServletException) ex;
            } else {
                throw new ServletException(ex);
            }
        }
    }

    private void printEventForm(PrintWriter out) {
        out.println("<h2>Add new event:</h2>");
        out.println("<form>");
        out.println("Title: <input name='eventTitle' length='50'/><br/>");
        out.println("Date (e.g. 24.12.2009): <input name='eventDate' length='10'/><br/>");
        out.println("<input type='submit' name='action' value='store'/>");
        out.println("</form>");
    }

    private void listEvents(PrintWriter out,
                            DateFormat dateFormatter) {

        List result = HibernateUtil.getSessionFactory()
                .getCurrentSession().createCriteria(Event.class).list();
        if (result.size() > 0) {
            out.println("<h2>Events in database:</h2>");
            out.println("<table border='1'>");
            out.println("<tr>");
            out.println("<th>Event title</th>");
            out.println("<th>Event date</th>");
            out.println("</tr>");
            for (Object aResult : result) {
                Event event = (Event) aResult;
                out.println("<tr>");
                out.println("<td>" + event.getTitle() + "</td>");
                out.println("<td>" + dateFormatter.format(event.getDate()) + "</td>");
                out.println("</tr>");
            }
            out.println("</table>");
        }
    }

    private void listEvents(PrintWriter out,
                            SimpleDateFormat dateFormatter) {

        List result = HibernateUtil.getSessionFactory()
                .getCurrentSession().createCriteria(Event.class).list();
        if (result.size() > 0) {
            out.println("<h2>Events in database:</h2>");
            out.println("<table border='1'>");
            out.println("<tr>");
            out.println("<th>Event title</th>");
            out.println("<th>Event date</th>");
            out.println("</tr>");
            Iterator it = result.iterator();
            while (it.hasNext()) {
                Event event = (Event) it.next();
                out.println("<tr>");
                out.println("<td>" + event.getTitle() + "</td>");
                out.println("<td>" + dateFormatter.format(event.getDate()) + "</td>");
                out.println("</tr>");
            }
            out.println("</table>");
        }
    }

    protected void createAndStoreEvent(String title,
                                       Date theDate) {
        Event theEvent = new Event();
        theEvent.setTitle(title);
        theEvent.setDate(theDate);

        HibernateUtil.getSessionFactory()
                .getCurrentSession().save(theEvent);
    }
}
