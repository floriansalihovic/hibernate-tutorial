package org.hibernate.tutorial.util;

import org.hibernate.*;
import org.hibernate.boot.registry.*;
import org.hibernate.cfg.*;

/**
 * Created by florian on 18/04/14.
 */
public class HibernateUtil {

    private static final SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory() {
        final Configuration configuration = new Configuration();

        return configuration
                .configure()
                .buildSessionFactory(
                        new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build());
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
