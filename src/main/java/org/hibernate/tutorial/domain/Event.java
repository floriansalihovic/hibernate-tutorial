package org.hibernate.tutorial.domain;

import java.util.*;

/**
 * Created by florian on 18/04/14.
 */
public class Event {

    private Long id;

    private String title;

    private Date date;

    private Set<Person> participants;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Set<Person> getParticipants() {
        return participants;
    }

    public void setParticipants(Set<Person> participants) {
        this.participants = participants;
    }

    public Event addParticipant(final Person person) {
        this.getParticipants().add(person);
        return this;
    }

    public Event removeParticipant(final Person person) {
        this.getParticipants().remove(person);
        return this;
    }
}
