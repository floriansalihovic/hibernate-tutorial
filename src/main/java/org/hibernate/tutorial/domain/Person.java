package org.hibernate.tutorial.domain;

import java.util.*;

/**
 * Created by florian on 18/04/14.
 */
public class Person {

    private Long id;

    private int age;

    private String firstName;

    private String lastName;

    private Set<Event> events;

    private Set<String> emailAddresses;

    public Person() {
        emailAddresses = new HashSet<String>();
        events = new HashSet<Event>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Set<Event> getEvents() {
        return events;
    }

    public void setEvents(Set<Event> events) {
        this.events = events;
    }

    public Set<String> getEmailAddresses() {
        return emailAddresses;
    }

    public void setEmailAddresses(Set<String> emailAddresses) {
        this.emailAddresses = emailAddresses;
    }

    public Person addToEvent(final Event event) {
        this.getEvents().add(event);
        event.addParticipant(this);
        return this;
    }

    public Person removeFromEvent(final Event event) {
        this.getEvents().remove(event);
        event.removeParticipant(this);
        return this;
    }
}
